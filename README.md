# Python Automation Testing
## Prerequisite
* Chrome Driver
* Chrome Browser with same configration(version of Chrome Driver)

## Basic Installtion
```
python
```

```
easy_install pip
```

```
pip install selenium
```
## Start Test
1. Open the file known as Automation_sample.py
2. Run this class or type in terminal **python Automation_sample.py**
3. Browser load automatically and your test will be start.
4. you can see your desired results on browser.

